﻿using SkiaSharp;

class Program
{
    readonly SKColor red   = new(192, 0,   0,   128);
    readonly SKColor green = new(0,   128, 0,   192);
    readonly SKColor blue  = new(0,   0,   128, 192);

    readonly SKColor nextzone  = new( 96,   128, 0, 192);
    readonly SKColor redzone   = new(128,   0,   0, 128);
    readonly SKColor deadzone  = new(128,   0,   0, 42);

    static readonly string inputImagePath = "./input.jpg";
    static readonly string outputImagePath = "./Ausgangsbild.jpg";
    static readonly SKBitmap baseImage = SKBitmap.Decode(inputImagePath);

    static float boarerDistance = 0.2f;
    static readonly bool verbose = true;

    float xOffset = 0;
    float yOffset = 0;
    
    static void Main()
    {
        Program run = new();
        run.MainLoop();
    }

    void MainLoop() {

        // RenderTest();

        float[,] its = new float[,] {
            { 2f,    0f,    0f },
            { 2f,    0f,    0f },
            { 0.88f, 0.00f, 0.00f },
            { 0.42f, 0.20f, 0.20f },
            { 0.23f, 0.00f, 0.20f }
        };

        RoundData[] rdata = GenerateRoundData(7, 0.05f, 0.95f);

        float xChange = 0f, yChange = 0f;
        foreach(RoundData current in rdata){
            Console.WriteLine(current);
            xChange += current.X;
            yChange += current.Y;
        }
        Log($"Total Chagne : [x = {xChange}, y = {yChange}]");

        Render(rdata);
    }

    void RenderTest() {
        using SKBitmap outputImage = SKBitmap.Decode(inputImagePath);

        Draw(outputImage, nextzone, 0.05f, 0.1f, 0f);
        Draw(outputImage, redzone,  0.42f, 0.1f, -0.2f);
        Draw(outputImage, deadzone, 0.88f, 0.2f, -0.2f);

        PrintImage(outputImage, outputImagePath);
    }

    RoundData[] GenerateRoundData(int iterations, float minR, float maxR) {
        RoundData[] rdata = new RoundData[iterations+1];
        rdata[0] = new RoundData(0f, 0f, 2f);

        float totalWeight = TotalWeight(iterations) * (1 + boarerDistance);
        float weightSum = 0f;

        float r = maxR;
        float rChange = (maxR - minR) / (iterations -1);

        Random random = new Random();

        for(int i = 1; i < iterations+1; i++) {
            int weight = Weight(iterations -i);
            float relativeWeight = weight / totalWeight;

            Log($"Iteration {i} [weight = {weight} : relative weight = {relativeWeight}]");
            
            float x = (float)(random.NextDouble() * 2 - 1) * relativeWeight;
            float y = (float)(random.NextDouble() * 2 - 1) * relativeWeight;

            rdata[i] = new RoundData( x, y, r);

            weightSum += relativeWeight;
            r -= rChange;
        }
        Log($"Relative weight sum : {weightSum}");

        return rdata;
    }

    int Weight(int iterations) {
        int weight = 0;
        for(int i = 1; i < iterations+1; i++) {
            weight += i*i;
        }
        return weight;
    }

    int TotalWeight(int iterations) {
        int totalWeight = 0;
        for(int i = 0; i < iterations; i++) {
            totalWeight += Weight(i);
        }
        return totalWeight;
    }

    void Render(RoundData[] rdata) {
        PrintImage(SKBitmap.Decode(inputImagePath), "./render/0-start.jpg");

        for(int i = 1; i < rdata.Length; i++) {
            RenderRound(rdata, i);
        }
    }

    void RenderRound(RoundData[] rdata, int i) {
        RenderNext(rdata, i);
        RenderDead(rdata, i);
        xOffset += rdata[i].X;
        yOffset += rdata[i].Y;
    }

    void RenderNext(RoundData[] rdata, int i) {
        using SKBitmap outputImage = SKBitmap.Decode(inputImagePath);

        Draw(outputImage, nextzone, rdata[i].X, rdata[i].Y, rdata[i].R);
        i -= 1;
        Draw(outputImage, redzone,  rdata[i].X, rdata[i].Y, rdata[i].R);

        PrintImage(outputImage, $"./render/{i+1}_0-next.jpg");
    }

    void RenderDead(RoundData[] rdata, int i) {
        using SKBitmap outputImage = SKBitmap.Decode(inputImagePath);

        Draw(outputImage, redzone, rdata[i].X, rdata[i].Y, rdata[i].R);

        PrintImage(outputImage, $"./render/{i}_1-dead.jpg");
    }

    void Draw(SKBitmap image, SKColor newColor, float ox, float oy, float r)
    {
        int width  = image.Width;
        int height = image.Height;
        
        // Koordinaten für den Mittelpunkt des invertierten Kreises
        float centerX = width  / 2f;
        float centerY = height / 2f;
        centerX += centerX * (ox + xOffset);
        centerY += centerY * (oy + yOffset);

        float radius = Math.Min(width, height) * r / 2;

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                float distance = (float) Math.Sqrt(Math.Pow(x - centerX, 2) + Math.Pow(y - centerY, 2));

                if (distance > radius)
                {
                    SKColor currentColor = baseImage.GetPixel(x, y);
                    SKColor drawColor = blendColor(currentColor, newColor);

                    image.SetPixel(x, y, drawColor);
                }
            }
        }
    }

    SKColor blendColor(SKColor currentColor, SKColor modColor) {
        return new SKColor(
            (byte) (((currentColor.Red   + modColor.Red)   / 2) * modColor.Alpha / 256),
            (byte) (((currentColor.Green + modColor.Green) / 2) * modColor.Alpha / 256),
            (byte) (((currentColor.Blue  + modColor.Blue)  / 2) * modColor.Alpha / 256)
        );
    }

    void PrintImage(SKBitmap image, string outputPath) {
        using (SKImage modifiedImage = SKImage.FromBitmap(image))
        using (SKData data = modifiedImage.Encode())
        using (System.IO.Stream stream = System.IO.File.OpenWrite(outputPath))
        {
            data.SaveTo(stream);
        }

        Console.WriteLine("Save image : " + outputPath);
    }

    private void Log(String message) {
        if(verbose) {
            Console.WriteLine(message);
        }
    }

}
