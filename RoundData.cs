
public readonly struct RoundData {

    public RoundData(float x, float y, float r) {
        X = x;
        Y = y;
        R = r;
    }

    public float X { get; }
    public float Y { get; }
    public float R { get; }

    public override string ToString() => $"(x = {X}, y = {Y}), r = {R}";
}